//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Thomas Sanger Borthwick on 10/2/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.h"
#include <cmath>


MidiMessage::MidiMessage()//Constructor
{
    number=60;
    velocity = 0;
    channel = 0;
}

MidiMessage::~MidiMessage()//Destructor
{
    
}
void MidiMessage::SetNoteNumber(int value) //Modifier
{
    if(value <= 127 && value >= 0)
        number = value;
}

int MidiMessage::GetNoteNumber() const //Accessor
{
    return number;
}

float MidiMessage::GetMidiNoteInHertz() const //Accessor
{
    return 440 * pow(2, (number-69)/12.0);
}

float MidiMessage::GetFloatVelocity() const
{
    return velocity;
}

void MidiMessage::SetFloatVelocity(float value)
{
    if(value <= 127 && value >= 0)
        velocity = value;
}

void MidiMessage::SetChannelNumber(int value)
{
    if (value <= 16 && value >= 0)
        channel = value;
}

float MidiMessage::getChannelNumber() const
{
    return channel;
}
