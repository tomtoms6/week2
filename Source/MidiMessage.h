//
//  MidiMessage.h
//  CommandLineTool
//
//  Created by Thomas Sanger Borthwick on 10/2/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__MidiMessage__
#define __CommandLineTool__MidiMessage__

#include <iostream>

class MidiMessage
{
public:
    
    /** Constuctor (initilising)*/
    MidiMessage();
    
    /** Destructor     Currently unused*/
    ~MidiMessage();//Destructor
    
    /** Sets the note number from inputed MIDI notes*/
    void SetNoteNumber(int number); //Mutator
   
    /** Gets current Midi Note Number*/
    int GetNoteNumber() const; //Accessor
   
    /** Calculates frequency of midi note*/
    float GetMidiNoteInHertz() const; //Accessor
  
    /** gets current velocity*/
    float GetFloatVelocity() const;
   
    /** sets velocity from inputed MIDI data*/
    void SetFloatVelocity(float velocity);
   
    /** sets channel number from inputed MIDI data*/
    void SetChannelNumber(int channel);
    
    /** gets current channel number*/
    float getChannelNumber() const;
    
    
private:
    int number, channel;
    float velocity;
};


#endif /* defined(__CommandLineTool__MidiMessage__) */
