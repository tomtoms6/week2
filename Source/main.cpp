//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "MidiMessage.h"

int main (int argc, const char* argv[])
{

    MidiMessage note;
    int noteNum, velocity, channel;
    
    std::cout << "Hello, World!\nPlease Enter NoteNumber, velocity and channel\n";
    
    std::cin >> noteNum;
    note.SetNoteNumber(noteNum);
    std::cin >> velocity;
    note.SetFloatVelocity(velocity);
    std::cin >> channel;
    note.SetChannelNumber(channel);
    
    std::cout << note.GetNoteNumber() << " = Note Number\n"<< note.GetMidiNoteInHertz() << " = Frequency\n" << note.GetFloatVelocity() << " = Velocity\n" << note.getChannelNumber() << " = Channel\n";

    
    return 0;
}

